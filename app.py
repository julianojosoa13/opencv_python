import cv2 as cv
import matplotlib.pyplot as plt

def main():
    img_name = 'moi.png'

    img = cv.imread(img_name, 1)

    #cv.cvtColor(img, cv.COLOR_BGR2RGB)

    b,g,r = cv.split(img)       # get b,g,r
    img = cv.merge([r,g,b])     # switch it to rgb

    plt.imshow(img)
    plt.title("Image Original")

    plt.show()

if __name__=='__main__':
    main()
